import sun.audio.AudioPlayer
import java.util.Scanner

var arrOX = arrayOf(
    arrayOf(' ', '1', '2', '3'),
    arrayOf('1', '-', '-', '-'),
    arrayOf('2', '-', '-', '-'),
    arrayOf('3', '-', '-', '-')
)
var player = 'X'
var round = 0
var flag = true
fun main() {
    val kb = Scanner(System.`in`)
    showWelcome()
    showTable()
    while (flag) {
        showTurn(round)
        var inputRow = kb.next();
        var inputCol = kb.next();
        try{
            setTable(inputRow.toInt(), inputCol.toInt(), player)
        }catch (e: Throwable){
            println("Fail your stupid press")
        }
        showTable()
        checkHorizantal(player)
        if(flag){
            checkVertical(player)
        }
        if(flag){
            chcekDiagonal(player)
        }
        checkDraw()
    }
}

fun showTurn(round: Int) {
    if (round % 2 == 0) {
        println("X turn")
        print("Please input your Row Col : ")
        player = 'X'
    } else {
        println("O turn")
        print("Please input your Row Col : ")
        player = 'O'
    }
}

fun showWelcome() {
    println("Welcome to OX Game")
}

fun showTable() {
    for (row in arrOX) {
        for (col in row) {
            print("$col ")
        }
        println()
    }
}
fun checkDraw(){
    if(round == 9){
        println("Draw")
        flag = false
    }
}

fun setTable(row: Int, col: Int, player: Char) {
    try {
        if (row > 3 || row < 1) {
            println("Fail your stupid press")
        } else if (col > 3 || col < 1) {
            println("Fail your stupid press")
        } else if(arrOX[row][col] != '-'){
            println("Fail your stupid press")
        }
        else {
            arrOX[row][col] = player
            round++
        }
    } catch (e: Throwable) {
        println("Fail your stupid press")
    }
}

fun checkWin(point: Int){
    if (point >= 3) {
        flag = false
        println("$player Win")
    }
}

fun checkHorizantal(player: Char) {
    var point = 0
    for (i in 1..3) {
        for (j in 1..3) {
            if (arrOX[i][j] == player) {
                point++
            }else{
                point = 0
            }
        }
        checkWin(point)
    }

}

fun checkVertical(player: Char) {
    var point = 0
    for (i in 1..3) {
        for (j in 1..3) {
            if (arrOX[j][i] == player) {
                point++
            }else{
                point = 0
            }
        }
       checkWin(point)
    }
}

fun chcekDiagonal(player: Char) {
    var point = 0
    for (i in 1..3) {
        if (arrOX[i][i] == player) {
            point++
        }else{
            point = 0
        }
    }
    checkWin(point)

    if(flag){
            var check = 3
            for (j in 1..3) {
                if (arrOX[check][j] == player) {
                    point++
                }else{
                    point = 0
                }
                check--
            }
        checkWin(point)
    }
}



